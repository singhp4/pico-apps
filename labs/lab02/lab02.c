//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.
#include "pico/stdlib.h"
#include <math.h>
#include "pico/double.h" 
#include "pico/float.h" 
#include <stdio.h>
#include <stdlib.h>




/**
 * @brief implements the Wallis product algorithm, that is 
 * described in the introduction, using double-precision
 * (double) floating-point representation and returns the 
 * calculated value for PI
 */
 void double_precision(double prod){

    double a;
    double b; 

    /**
     * @brief wallis product algorithm is implemented below
     * 
     */

    for (double j = 1; j <= 100000; j++){

        a = (2 * j) / ( (2 * j) - 1);
        //
        b = (2 * j) / ( (2 * j) + 1);

        prod *= a * b;
    }


    double cal = prod * 2;

    printf("Double precision floating-point Pi = %.15f\n", cal);

    double compare = 3.14159265359; 

    /**
     * @brief approximate error is calculated below
     * 
     */

    double approx = ( ( (cal - compare) / compare)*100 );

    if(approx>0){
      printf("Approximation Error Double = %.15f\n", approx);
    }
    else{
      approx*=-1;
      printf("Approximation Error Double = %.15f\n", approx);
    }

}  

/**
 * @brief implements the Wallis product algorithm, that is 
 * described in the introduction, using single-precision
 * (float) floating-point representation and returns the 
 * calculated value for PI
 * @return int  Application return code 
 */
void single_precision(float prod){
    float a, b; 

    /**
     * @brief wallis product algorithm is implemented below
     * 
     */
    for (float j = 1; j <= 100000; j++){
        a = (2 * j) / ( (2 * j) - 1);
        b = (2 * j) / ( (2 * j) + 1);

        prod *= a * b;
    }

    float cal = prod * 2;


    printf("Single precision floating-point Pi= %.7f\n", cal);

    float compare = 3.14159265359; 

    /**
     * @brief approximate error is calculated below
     * 
     */

    float approx_error = ( ( (cal - compare) / compare )*100 );

    if(approx_error>0){
      printf("Approximation Error Double = %.7f\n", approx_error);
    }
    else{
      approx_error*=-1;
      printf("Approximation Error Double = %.7f\n", approx_error);
    }
}  

int main() {

    #ifndef WOKWI
     // Initialise the IO as we will be using the UART
     // Only required for hardware and not needed for Wokwi
     stdio_init_all();
    #endif
    float prod_f = 1.0;
    double prod_d = 1.0;
    


    
    printf("Calculating value of Pi using single-precision (float) floating-point representation!\n");
    /**
    * calling single_precision function
    */ 
    single_precision(prod_f);

    
    printf("Calculating Pi using  double-precision (double) floating-point representation!\n");

    /**
    * calling double_precision function
    */ 
    double_precision(prod_d);
    


    // Returning zero means everything runs successfully.
    return 0;
}
