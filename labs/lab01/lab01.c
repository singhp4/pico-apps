#include "pico/stdlib.h"

/**
  * This subroutine takes LED_PIN number and LED_DELAY as  
  * input value. It will repeatedly toggle the LED on and  
  * off. 
  * 
  * LED_PIN takes the number of the led pin as input
  *
  * LED_DELAY takes the duration for which LED will be 
  * turned off
  *
  * 
  */
 void subroutine(const uint LED_PIN, const uint LED_DELAY){
     // Do forever
    while (true) {

        // Toggle the LED on and then sleep for delay period
        gpio_put(LED_PIN, 1);
        sleep_ms(LED_DELAY);

        // Toggle the LED off and then sleep for delay period
        gpio_put(LED_PIN, 0);
        sleep_ms(LED_DELAY);

    }

 }
int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Calling the subroutine to make our LED blink
    subroutine(LED_PIN,LED_DELAY);

    

    // Should never get here due to infinite while-loop.
    return 0;

}
